<?php
require_once('include.php');

use Zombie\Models as Models;

$page['title'] = '';
$page['scripts'] = [];
$zombies = new Models\Zombie();

$status = $zombies->getAllStatus();
require('Views/_head.phtml');
require('Views/registrar.phtml');
require('Views/_footer.phtml');