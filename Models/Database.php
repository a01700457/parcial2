<?php

namespace Zombie\Models;

class Database{

    private static $instance;

    private $con;

    private function __construct(){
        $host = 'localhost';
        $user = 'root';
        $pass = 'toor';
        $driver = 'mysql';
        $db = 'zombies';
        $this->con = new \PDO($driver . ':dbname=' . $db . ';host=' . $host, $user, $pass);
    }

    public static function getInstance(){
        if(!isset(self::$instance))
            self::$instance = new self();
        return self::$instance;
    }

    public function getConnection(){
        return $this->con;
    }

}
