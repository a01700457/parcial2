<?php

namespace Zombie\Models;

class Zombie{
    private $db;

    public function __construct(){
        $this->db = Database::getInstance()->getConnection();
    }

    public function create($zombie){
        $sql=<<<SQL
INSERT INTO zombie (name, status)
VALUES (:name, :status)
SQL;
        $stm = $this->db->prepare($sql);
        if($stm->execute($zombie))
            return $this->db->lastInsertId();
        return false;
    }

    public function getAllStatus(){
        $sql="SELECT * FROM status";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll(\PDO::FETCH_KEY_PAIR);
    }

    public function statistics(){
        $data = [];
        $sql=<<<SQL
SELECT s.name, COUNT(1) AS total
FROM zombie z, status s
WHERE z.status = s.id
GROUP BY s.name
SQL;
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data['byStatus'] = $stm->fetchAll(\PDO::FETCH_KEY_PAIR);

        $sql="SELECT COUNT(1) AS total FROM zombie";
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data['total'] = $stm->fetch(\PDO::FETCH_ASSOC)['total'];


        $sql=<<<SQL
SELECT COUNT(1) AS total
FROM zombie z, status s
WHERE z.status = s.id
    AND s.name != 'completamente muerto'
SQL;
        $stm = $this->db->prepare($sql);
        $stm->execute();
        $data['notDead'] = $stm->fetch(\PDO::FETCH_ASSOC)['total'];

        return $data;
    }

    public function getAllByDate(){
        $sql=<<<SQL
SELECT z.id, z.name, z.added, z.turned, s.name as status
FROM zombie z, status s
WHERE z.status = s.id
ORDER BY added DESC
SQL;
        $stm = $this->db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getByStatus($status){
        $sql=<<<SQL
SELECT z.name, z.added, z.turned
FROM zombie z, status s
WHERE z.status = s.id
    AND s.name = :status
ORDER BY added DESC
SQL;
        $stm = $this->db->prepare($sql);
        $stm->bindParam(':status', $status);
        $stm->execute();
        return $stm->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    public function getById($id){
        $sql=<<<SQL
SELECT z.id, z.name, s.id as status
FROM zombie z, status s
WHERE z.status = s.id
    AND z.id = :id
ORDER BY added DESC
SQL;
        $stm = $this->db->prepare($sql);
        $stm->bindParam(':id', $id);
        $stm->execute();
        return $stm->fetch(\PDO::FETCH_ASSOC);
    }

    public function updateStatus($data){
        $sql=<<<SQL
UPDATE zombie
SET turned = NOW(),
    status = :status
WHERE id = :id
SQL;
        $stm = $this->db->prepare($sql);
        $stm->execute($data);
    }
}