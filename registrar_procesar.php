<?php
require_once('include.php');

use Zombie\Models as Models;

$data = $_POST;

if(!isset($data['name']) || !isset($data['status']))
    header('Location:/registrar.php');
else{
    foreach($data as $k => $v){
        $data[':'.$k] = $v;
        unset($data[$k]);
    }
    $id = (new Models\Zombie())->create($data);
    if(!id){
        header('Location:/registrar.php');
    }else{
        header('Location:/todos.php');
    }
}