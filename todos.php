<?php
require_once('include.php');

use Zombie\Models as Models;

$page['title'] = 'Todos';
$page['scripts'] = [];
$data = (new Models\Zombie())->getAllByDate();

require('Views/_head.phtml');
require('Views/todos.phtml');
require('Views/_footer.phtml');