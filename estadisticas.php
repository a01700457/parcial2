<?php
require_once('include.php');

use Zombie\Models as Models;
$page['title'] = "Consultas";
$page['scripts'] = [];
$data = (new Models\Zombie())->statistics();

require('Views/_head.phtml');
require('Views/estadisticas.phtml');
require('Views/_footer.phtml');