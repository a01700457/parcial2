<?php
require_once('include.php');

use Zombie\Models as Models;

if(!isset($_GET['id'])){
    header('Location:/todos.php');
}
$id = $_GET['id'];
$page['title'] = 'Editar';
$page['scripts'] = [];
$zombies = new Models\Zombie();

$status = $zombies->getAllStatus();
$zombie = $zombies->getById($id);

require('Views/_head.phtml');
require('Views/editar.phtml');
require('Views/_footer.phtml');