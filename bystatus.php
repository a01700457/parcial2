<?php
require_once('include.php');

use Zombie\Models as Models;

if(!isset($_GET['status'])){
    die();
}

$page['title'] = "Consultas";
$page['scripts'] = [];
$data = (new Models\Zombie())->getByStatus($_GET['status']);
$status = htmlspecialchars($_GET['status']);
require('Views/bystatus.phtml');